import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {path :'', redirectTo : '/home', pathMatch : 'full'},
  { path: 'list', loadChildren: () => import('./pages/flowers/list/list.module').then(m => m.ListModule) }, 
  { path: 'new', loadChildren: () => import('./pages/flowers/new/new.module').then(m => m.NewModule) },
  { path: 'details', loadChildren: () => import('./pages/flowers/details/details.module').then(m => m.DetailsModule) },
  { path: 'edit', loadChildren: () => import('./pages/flowers/edit/edit.module').then(m => m.EditModule) },
  { path: 'home', loadChildren: () => import('./pages/flowers/home/home.module').then(m => m.HomeModule) },
  { path: 'register', loadChildren: () => import('./pages/register/register.module').then(m => m.RegisterModule) },
  { path: 'login', loadChildren: () => import('./pages/login/login.module').then(m => m.LoginModule) },
  { path: 'singup', loadChildren: () => import('./pages/singup/singup.module').then(m => m.SingupModule) },
  {path: '**', redirectTo: 'home'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
