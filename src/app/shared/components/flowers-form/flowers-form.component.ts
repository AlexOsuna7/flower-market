import { Component, OnInit } from '@angular/core';
import { AngularFireStorage } from '@angular/fire/storage';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { FlowersService } from 'src/app/pages/flowers/flowers.service';
import { FileI } from '../models/file.interface';
import { Flower } from '../models/flower.interface';

@Component({
  selector: 'app-flowers-form',
  templateUrl: './flowers-form.component.html',
  styleUrls: ['./flowers-form.component.scss']
})
export class FlowersFormComponent implements OnInit {
  flowerItem : Flower;
  flowerToSend : Flower;
  private image : any;
  private filePath : any;
  public url: Observable<string>;

  messages = {
    minLength : "Ingresa el minimo de caracteres",
    required : "Este campo es requerido"
  }

  flowerForm : FormGroup;
  constructor(private router : Router, 
    private fb : FormBuilder, 
    private flowerService : FlowersService,
    private toastr: ToastrService,
    private storage : AngularFireStorage) {

    const navigation = this.router.getCurrentNavigation();
    this.flowerItem = navigation?.extras?.state?.value;
    this.initForm();
  }

  ngOnInit(): void {

    if(typeof this.flowerItem === 'undefined'){
      //redirect
      this.router.navigate(['new']);
    }else {
      this.flowerForm.patchValue(this.flowerItem);
      this.url = this.flowerItem.url;
    }
  }

  private initForm() : void {
    this.flowerForm = this.fb.group({
      titulo : ['', [Validators.required, Validators.minLength(5)] ],
      descripcion : ['', [Validators.required,Validators.minLength(5)] ],
      precio : ['', [Validators.required] ],
      url : ['']
    });
  }

  isValidField (field : string) : string {
    const validatedField = this.flowerForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid' : validatedField.touched ? 'is-valid' : '';
  }

  async handleImage(event : any) {
    this.image = event.target.files[0];
    console.log('image', this.image);
    this.uploadImage(this.image);
  }


  onSave() : void {
    if(this.flowerForm.valid){
      this.flowerToSend = this.flowerForm.value;
      this.flowerToSend.url = this.url;
      const flowerId = this.flowerItem?.id || null;
      this.flowerService.onSaveFlower(this.flowerToSend, flowerId);
      this.flowerForm.reset();
      this.toastr.success('Se han guardado los cambios', 'Exito!');
      this.onGoBack();
    }
  }

  onGoBack() : void {
    this.router.navigate(['list']);
  }

  
  async uploadImage(image : FileI) {
    this.filePath = `images/${image.name}`;
    const fileRef = this.storage.ref(this.filePath);
    try {
      const task = this.storage.upload(this.filePath, image);
      await task.snapshotChanges()
      .pipe(
        finalize( () => {
          fileRef.getDownloadURL().subscribe( urlImage => {
            this.url = urlImage;
            console.log('URL service', urlImage);
          });
        })
      ).subscribe();
    } catch (error) {
      console.error(error);
    }
  }
}
