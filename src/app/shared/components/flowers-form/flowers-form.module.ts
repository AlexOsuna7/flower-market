import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReactiveFormsModule } from '@angular/forms';
import { FlowersFormComponent } from './flowers-form.component';
import { AngularFireStorageModule } from '@angular/fire/storage';


@NgModule({
  declarations: [FlowersFormComponent],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AngularFireStorageModule
  ],
  exports: [FlowersFormComponent]
})
export class FlowersFormModule { }
