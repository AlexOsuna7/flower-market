import { Observable } from "rxjs";
export interface Flower {
    id? : string;
    titulo : string;
    descripcion : string;
    precio : string;
    url? : Observable<string>;
}