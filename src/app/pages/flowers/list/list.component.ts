import { Component, OnInit } from '@angular/core';
import { NavigationExtras, Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Observable } from 'rxjs';
import { Flower } from 'src/app/shared/components/models/flower.interface';
import { FlowersService } from '../flowers.service';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss']
})
export class ListComponent implements OnInit {
  flowers = this.flowersService.flowers;
  page = 1;
  navigationExtras : NavigationExtras = {
    state : {
      value : null
    }
  }

  constructor(private router : Router, 
    private flowersService : FlowersService,
    private toastr: ToastrService) { }

  ngOnInit(): void {
  }

  onGoToEdit(item : any) : void {
    this.navigationExtras.state.value = item;
    this.router.navigate(['edit'], this.navigationExtras);
  }

  async delete(id : any) : Promise<void> {
    try {
      await this.flowersService.onDeleteFlower(id);
      this.toastr.error('Se ha eliminado el registro', 'Eliminado!');
    } catch (err) {
      console.log(err);
    }
  }
}
