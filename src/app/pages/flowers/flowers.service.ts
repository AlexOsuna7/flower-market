import { Injectable } from '@angular/core';
import { AngularFirestoreCollection } from '@angular/fire/firestore/collection/collection';
import { AngularFirestore } from '@angular/fire/firestore';
import { Observable } from 'rxjs';
import { Flower } from 'src/app/shared/components/models/flower.interface';
import { finalize, map } from 'rxjs/operators';
import { async, reject, resolve } from 'q';
import { FileI } from 'src/app/shared/components/models/file.interface';
import { AngularFireStorage } from '@angular/fire/storage';
import { promise } from 'protractor';

@Injectable({
  providedIn: 'root'
})
export class FlowersService {
  flowers : Observable<Flower[]>;
  private filePath : any;

  private flowersCollection : AngularFirestoreCollection<Flower>;

  constructor(
    private afs : AngularFirestore,
    private storage : AngularFireStorage
  ) {

    this.flowersCollection = afs.collection<Flower>('flowers');
    this.getFlowers();
  }

  /*
    Delete flower item
  */
  onDeleteFlower(idFlower : string): Promise<void>{
    return new Promise (async (resolve, reject) => {
      try {

        const result = await this.flowersCollection.doc(idFlower).delete();
        resolve(result);

      } catch (err) {
        reject(err.messsage);
      }
    });
  }

  /*
    Save or edit flower item
  */
  onSaveFlower(flower : Flower, idFlower : string): Promise<void>{
    return new Promise(async(resolve, reject) => {
      try{
        const id = idFlower || this.afs.createId();
        const data = { id, ...flower};
        console.log(data);
        const result = this.flowersCollection.doc(id).set(data)
        resolve(result);

      }catch(err){
        reject(err.messsage);
      }
    });
  }
  /*
    Get flower items
  */
  private getFlowers(): void{
    this.flowers = this.flowersCollection.snapshotChanges().pipe(
      map(actions => actions.map(a => a.payload.doc.data() as Flower))
    );
  }
}
