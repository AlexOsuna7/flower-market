import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  image = "https://firebasestorage.googleapis.com/v0/b/flowers-db.appspot.com/o/home%2Fflower.jpg?alt=media&token=87cf1b6f-9a6f-4ba0-82a9-fe988b67e1d8";
  constructor() { }

  ngOnInit(): void {
  }

}
