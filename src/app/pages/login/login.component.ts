import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {

  constructor(private authService : AuthService, private router: Router) { }

  loginForm = new FormGroup ({
    email : new FormControl('', [Validators.required, Validators.email]),
    password : new FormControl('', Validators.required)
  });


  ngOnInit(): void {
  }

  isValidField (field : string) : string {
    const validatedField = this.loginForm.get(field);
    return (!validatedField.valid && validatedField.touched)
    ? 'is-invalid' : validatedField.touched ? 'is-valid' : '';
  }

  async onSave() {
    const {email, password} = this.loginForm.value;
    try {
      const user = await this.authService.login(email, password);
      if (user) {
        //redirect to home page
        this.router.navigate(['/home']);
      }
    } catch (error) {
      console.log(error);
    }
  }
}
