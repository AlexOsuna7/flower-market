// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.

export const environment = {
  production: false,
  firebaseConfig : {
    apiKey: "AIzaSyDtmExKJ2E6OXOehZgfLcV4j4iEIf1PDgI",
    authDomain: "flowers-db.firebaseapp.com",
    projectId: "flowers-db",
    storageBucket: "flowers-db.appspot.com",
    messagingSenderId: "371793955807",
    appId: "1:371793955807:web:2edad23b9582bd97fb926a"
  }
};

/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.
